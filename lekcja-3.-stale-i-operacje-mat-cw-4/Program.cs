﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lekcja_3._stale_i_operacje_mat_cw_4
{
    class Program
    {
        static void Main(string[] args)
        {
            const float PI = 3.14f;

            Console.WriteLine("Program obliczający obwód koła!");
            Console.WriteLine("Podaj długość promienia: ");
            int promien = int.Parse(Console.ReadLine());

            float obwod = 2 * PI * promien;
            Console.WriteLine("Obwód koła wynosi: {0}", obwod);

            Console.ReadKey();
        }
    }
}
